const sendRequest = (type, url) => {
    return new Promise((resolve, reject) => {
        const request = new XMLHttpRequest()
        request.open(type, url)
        request.send()
        request.responseType = 'json'
        request.onload = function () {
            resolve(request.response)
        }
    })
}

async function dataOutput(type, url) {
    const requestReceived = sendRequest(type, url)
    requestReceived.then(async movies => {
        console.log(movies);

        const data = await Promise.all(movies.map(async movie => {
            return [movie, await Promise.all(movie.characters.map(character => sendRequest("GET", character)))]
        }))

        display(data)
    })
}

function display(data) {
    for (let episodeData of data) {
        const episode = episodeData[0];
        console.log(episode)
        let list = document.createElement('ul')
        document.body.append(list)
        list.append(`Episode ${episode.episodeId}`)
        let characters = episodeData[1];
        for (let character of characters) {
            let listItem = document.createElement('li')
            listItem.innerText = character.name
            list.append(listItem)
        }
        let listItem = document.createElement('li')
        listItem.innerText = episode.name
        list.append(`Episod name: ${listItem.innerText}`)
        listItem = document.createElement('li')
        listItem.innerText = episode.openingCrawl
        list.append(listItem)
    }
}

const request = dataOutput('GET', 'https://ajax.test-danit.com/api/swapi/films')